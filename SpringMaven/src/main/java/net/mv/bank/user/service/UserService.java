package net.mv.bank.user.service;

import net.mv.bank.user.dto.UserDto;

public interface UserService {
	
	public UserDto authenticateUser(UserDto userDto);
	public void registerUser(UserDto userDto);

}
